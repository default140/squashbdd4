Feature: Connexion à application jpetstore v2

Scenario Outline: D2-E1
		Given un navigateur est ouvert
		When je suis sur url
		And je clique sur le lien de connexion
		And rentre le username <login>
		And rentre le password <password>
		And clique sur le bouton login
		Then utilisateur est connecté
		And je peux lire le message accueil <returnMessage>

		@AdminProfil
		Examples:
		| login | password | returnMessage |
		| "j2ee" | "j2ee" | "Welcome ABC!" |

		@UserProfil
		Examples:
		| login | password | returnMessage |
		| "ACID" | "ACID" | "Welcome ACID!" |
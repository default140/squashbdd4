package gherkinJava;

import static org.junit.Assert.assertTrue;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class jpetstore {
	
	WebDriver driver;
	
	@Given("un navigateur est ouvert")
	public void un_navigateur_est_ouvert() {
		System.setProperty("webdriver.gecko.driver", "./rsc/geckodriver.exe");
		driver = new FirefoxDriver();
	    driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
	    //throw new io.cucumber.java.PendingException();
	}

	@When("je suis sur url")
	public void je_suis_sur_url() {
	    driver.get("https://petstore.octoperf.com/actions/Catalog.action");
	    //throw new io.cucumber.java.PendingException();
	}

	@When("je clique sur le lien de connexion")
	public void je_clique_sur_le_lien_de_connexion() {
	    driver.findElement(By.xpath("//*[@id=\"MenuContent\"]/a[2]")).click();
	    //throw new io.cucumber.java.PendingException();
	}

	@When("rentre le username {string}")
	public void rentre_le_username(String string) {
		driver.findElement(By.name("username")).sendKeys(string);
	    //throw new io.cucumber.java.PendingException();
	}

	@When("rentre le password {string}")
	public void rentre_le_password(String string) {
		driver.findElement(By.name("password")).clear();
		driver.findElement(By.name("password")).sendKeys(string);
	    //throw new io.cucumber.java.PendingException();
	}

	@When("clique sur le bouton login")
	public void clique_sur_bouton_login() {
		driver.findElement(By.xpath("//*[@id=\"Catalog\"]/form/input")).click();
	    //throw new io.cucumber.java.PendingException();
	}

	@Then("utilisateur est connecté")
	public void utilisateur_est_connecte() {
	    assertTrue(true);
	    //throw new io.cucumber.java.PendingException();
	}

	@Then("je peux lire le message accueil {string}")
	public void je_peux_lire_le_message_accueil(String string) {
	    // Write code here that turns the phrase above into concrete actions
	    //throw new io.cucumber.java.PendingException();
	}


}
# language: en
Feature: D2-E2

	Scenario: D2-E2
		Given je suis connecté sur la page de prise de rendez-vous
			| login| John Doe|
			| password| ThisIsNotAPassword|
		When je renseigne les informations obligatoires
		And je clique sur Book Appointment
		Then le rendez-vous est confirmé


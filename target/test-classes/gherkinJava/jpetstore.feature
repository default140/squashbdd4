Feature: Connexion à application jpetstore

Scenario: Connexion
Given un navigateur est ouvert
When je susi sur url
And je clique sur le lien de connexion
And rentre le username "j2ee"
And rentre le password "j2ee"
And clique sur bouton login
Then utilisateur ABC est connecte
And je peux lire le message accueil "Welcome ABC!"
package gherkinJava;

import static org.junit.Assert.assertTrue;

import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class cura {
	
	WebDriver driver;

	
	@Given("je suis connecté sur la page de prise de rendez-vous")
	public void je_suis_connecté_sur_la_page_de_prise_de_rendez_vous(Map<String, String> cred) {
		System.setProperty("webdriver.gecko.driver", "./rsc/geckodriver.exe");
		driver = new FirefoxDriver();
	    driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
	    driver.get("https://katalon-demo-cura.herokuapp.com/profile.php#login");
	    driver.findElement(By.id("txt-username")).sendKeys(cred.get("login"));
	    driver.findElement(By.id("txt-password")).sendKeys(cred.get("password"));
	    driver.findElement(By.id("btn-login")).click();
	}

	@When("je renseigne les informations obligatoires")
	public void je_renseigne_les_informations_obligatoires() {
		assertTrue(true);
	}

	@When("je clique sur Book Appointment")
	public void je_clique_sur_Book_Appointment() {
		assertTrue(true);
	}

	@Then("le rendez-vous est confirmé")
	public void le_rendez_vous_est_confirmé() {
		assertTrue(true);
	}



}